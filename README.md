# Duke.edu in OKD

The eventual goal of this project is to get Duke.edu, both front-ends, backends, and some method to sync content, into a multi-cluster environment.

## Current status

This project is in Development.  Overall steps will be to first migrate front-ends to OKD using the legacy sync method, followed by a new backend, updated builds for backend data, and replacement of the front-ends.  Finally, some multi-cluster/multi-datacenter setup will be setup.

IN PROGRESS: Setup CronJobs for assets/static content

COMPLETED:

*  CronJob for main content
*  Web frontend with Nginx

TODO:

*  Stats (monitor rsync, etc, see below)
*  Test front-end scale out
*  Add redirects and other "meta" web config to configMaps and include configMaps in pod for web
*  What is up with the alert bar?  it's there included directly? no need for static file?
*  Sync validation; and bomb out if we fail - don't symlink bad content (test this)

ISSUES:

*   need to convert app.py to use pathlib for symlinking abilit

## CronJob container for legacy data sync

The service uses Kubernetes cron jobs to sync data from the backend server to a persistentVolume.  The resources for these cron jobs are labeled `component: sync`.

Some specific, important details of the sync:

1.  `concurrencyPolicy: Replace` is set for all cronJobs, with the assumption that a still-running container is in the middle of an rsync, and therefore the sync will pick up where it left off.
2.  The cronJob replaces the symlink to the site's DocumentRoot when the rsync has completed.
3.  Given the above, the rsync uses `--link-dest` as an option to hard-link content that has already been copied, therefore preventing the need to re-sync everything on each run, but preserving changes between syncs before the symlink is created (See: "Link Dest Specifics", below)
4.  There is an individual cronJob to sync each of: site content, asset data, static content and alerts.


### Link Dest Specifics

The rsync option `--link-dest=` specifies a path on the destination host, relative to the directory into which content is being copied, which contains data that has already been synced.  If any of this pre-synced content exists, and is the same as the remote source, then rsync creates a hard-link to the file in the sync directory.  This prevents rsync from having to re-sync data that is the same, and allows for "snapshot" structures.

Example:  Given the directory structure below, if rsync is syncing remote content into `sync-destination/` and existing, pre-synced content exists in `link-source`, the argument would be `--link-dest=../link-source`.

```
data/
  sync-destination/
  link-source/
```

For this application, the previous DocumentRoot is specified as the link dest source, rsync created hard-links into a directory named for the current Unix epoch time, and then syncs any new or changed data.  When this completes, the link to the previous DocumentRoot is removed, and re-created pointing to the new sync data.

This created a directory structure that contains all the content for Duke.edu at that point in time (snapshot), but only required rsync to copy a given file version once, and effectively de-duplicates the destination.  Sync time and resource usage is significantly reduced.

### duke-edu Service Account and Vault integration

The `duke-edu` service account in the `duke-edu` project is setup to access `https://vault-automation.oit.duke.edu` to retrieve login credentials for backend Duke.edu staging environment.  This used the Automation Vault's `openshift` Kubernetes authentication endpoint, `duke_frontends` role, and `duke-edu` policy to access the `duke_frontends` secret.

### Notes

The sync will be a python script, using rsync and utilizing --link-dir to get snapshotty backups from the destination and the last backup. If the last backup doesn't exist, a full one is done.

Cleanup is done if FS fills





## TODOS:

Monitor:

1.  cronJobs run duration
2.  cronJobs files transferred
3.  failed vs succeeded cronJobs
4.  other Rsync data?

Create Prometheus endpoint?  Or Python to Influxdb?

Log or set current Symlink location?



Final:

1.  Add annotation do documentation when completed
2.  Change Git repos from clc0317 to official repo in buildConfigs
3.  Setup Gitlab webhook triggers for buildConfigs using ^^ Those repos
4.  Rename route hostname for duke.edu
