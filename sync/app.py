#!/usr/bin/env python3

import os
import sys
import hvac
import time
import subprocess


def debug(message):
    """Log messages to stdout if debug is set."""

    if os.getenv('DEBUG'):
        print("[DEBUG] {}".format(message))


def parseVariables(required_variables):
    """Check if required environment variables are set, or bomb out."""

    output_hash = {}

    for key in required_variables:
        debug("Checking key: {}".format(key))
        value = os.getenv(key)
        debug("Got vaule: {}".format(value))
        if key is None:
            print("[ERROR] {} environment variable must be set.".format(key))
            sys.exit(1)
        else:
            output_hash[key] = value

    debug("Returning hash: {}".format(output_hash))
    return output_hash


def syncContents(info):
    """Sync content from source to local volume."""

    date = str(time.time())
    date_human = str(time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime()))
    user = info['USERNAME']
    host = info['HOSTNAME']
    key = info['KEY']
    source = info['SOURCE_PATH']
    destination = "{path}/{date}".format(path=info['DESTINATION_PATH'],
                                         date=date)

    source_string = '{}@{}:{}'.format(user, host, source)
    key_file = '/tmp/key'

    debug("Writing key file")
    with open(key_file, 'w+') as f:
        f.write(key)

    os.chmod(key_file, 0o600)
    debug("Syncing content")

    subprocess.run(
        ['/usr/bin/rsync',
         '--archive',
         '--compress',
         '--verbose',
         '--link-dest={}'.format('../current'),
         '-e',
         'ssh -o StrictHostKeyChecking=no -q -i {}'.format(key_file),
         source_string,
         destination]
    )

    debug("Writing time file")
    with open("{dest}/time.txt".format(dest=destination), 'w+') as f:
        f.write(date_human)

    return destination


def vaultLogin(creds):
    """Log into vault and return the client."""
    debug("Using Kubernetes token for Vault authentication")

    vault_client = hvac.Client(creds["VAULT_ADDR"])

    token_file = '/var/run/secrets/kubernetes.io/serviceaccount/token'

    with open(token_file) as f:
        token = f.read()

    vault_client.auth_kubernetes(creds["VAULT_ROLE"],
                                 token,
                                 mount_point=(creds["VAULT_ENDPOINT"]))

    assert vault_client.is_authenticated(), "Failed to authenticate to Vault."

    return vault_client


def main():
    """Test Vault integration."""

    vault_credentials = parseVariables(["VAULT_ADDR",
                                        "VAULT_ENDPOINT",
                                        "VAULT_ROLE"])

    sync_info = parseVariables(["USERNAME",
                                "HOSTNAME",
                                "SOURCE_PATH",
                                "DESTINATION_PATH"])

    client = vaultLogin(vault_credentials)

    debug("Retrieving id_rsa")
    vault_response = client.secrets.kv.v2.read_secret_version(
        path='duke-edu/sync')

    sync_info['KEY'] = vault_response['data']['data']['id_rsa']
    debug("Received id_rsa file...")

    sync_path = syncContents(sync_info)

    current_dir = '{}/current'.format(sync_info['DESTINATION_PATH'])

    if os.path.islink(current_dir):
        os.unlink(current_dir)
    os.symlink(os.path.basename(sync_path), current_dir)


if __name__ == '__main__':
    main()
